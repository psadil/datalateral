# data-lateral

data for lateral connections experiments

To produce data, the gitsubmodules must be initiated. Then, the file file `data-raw/collect_visr-lateral.R`
can be run. This assumes that the `tidyverse` and `magrittr` have been installed.

None of that needs to be done if the goal is just to use the data. To install:

```R
install.packages("remotes")
remotes::install_gitlab("psadil/datalateral")
```

Then, the files can be loaded into the environment with

```R
data("lateral", package = "datalateral")
```
